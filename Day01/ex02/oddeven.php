#!/usr/bin/php
<?php
while (1)
{
	echo "Enter a Number: ";
	$input = fgets(STDIN);
	if ($input === false)
	{
		echo "\n";
		break;
	}
	$input = str_replace("\n", "", $input);
	if (ctype_digit($input))
	{
		$int = (int)$input;
		if ($int % 2 == 0)
			echo ("The number $int is even\n");
		else
			echo ("The number $int is odd\n");
	}
	else
		echo "'$input' is not a number\n";
}
