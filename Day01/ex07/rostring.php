#!/usr/bin/php
<?php

function ft_split($string) 
{
	$array = explode(" ", $string);
	$final = array();
	foreach ($array as $line)
	{
		if (!$line == "")
			array_push($final, $line);
	}
	return $final;
}

if ($argv[1])
{
	$test = "";
	$arr = ft_split($argv[1]);
	$first = $arr[0];
	$final = array_splice($arr, 1);
	array_push($final, $first);
	foreach ($final as $line)
		$test .= "$line ";
	$test = rtrim($test);
	$test .= "\n";
	echo $test;
}