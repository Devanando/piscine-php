#!/usr/bin/php
<?php
function devan_sort($a, $b)
{
	$a = strtolower($a);
	$b = strtolower($b);
	for ($i=0;; $i++)
	{
		if ($a[$i] == "" || $b[$i] == "")
			return ($a[$i] < $b[$i]) ? -1 : 1;
		if ($a[$i] == $b[$i])
			continue;
		if (ctype_alpha($a[$i]) && ctype_alpha($b[$i]))
			return ($a[$i]< $b[$i]) ? -1 : 1;
		if (ctype_alpha($a[$i]) && !ctype_alpha($b[$i]))
			return -1;
		if (!ctype_alpha($a[$i]) && ctype_alpha($b[$i]))
			return 1;
		if (ctype_digit($a[$i]) && ctype_digit($b[$i]))
			return ($a[$i] < $b[$i]) ? -1 : 1;
		if (!ctype_digit($a[$i]) && ctype_digit($b[$i]))
			return 1;
		if (ctype_digit($a[$i]) && !ctype_digit($b[$i]))
			return -1;
		else
			return ($a[$i] < $b[$i]) ? -1 : 1;
	}
}

$next = [];
$i = 0;
foreach ($argv as $arg)
{
	if ($i == 0)
		$i++;
	else
	{
		$array = explode(" ", $arg);
		foreach ($array as $part)
		{
			if (!strlen($part) < 1)
				array_push($next, $part);
		}
	}
}
usort($next, "devan_sort");
foreach ($next as $line)
	echo "$line\n";

?>