#!/usr/bin/php
<?php
$num1 = (int)trim($argv[1], ' \t');
$op = trim($argv[2], ' \t');
$num2 = (int)trim($argv[3], ' \t');
if ($argv[1] == "" || $argv[2] == "" ||$argv[3] == "" || strlen($argv[4]) > 0)
	echo "Incorrect Parameters\n";
else
{
	if ($op == '+')
		$final = $num1 + $num2;
	else if ($op == '-')
		$final = $num1 - $num2;
	else if ($op == '*')
		$final = $num1 * $num2;
	else if ($op == '/')
		$final = $num1 / $num2;
	else if ($op == '%')
		$final = $num1 % $num2;
	echo "$final\n";
}