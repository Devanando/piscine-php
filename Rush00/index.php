
<!DOCTYPE html>
<html>
    <head>
        <title>landing page</title>
        <link rel="stylesheet" type="text/css" href="css/landing_page.css">
        <link rel="stylesheet" type="text/css" href="css/global.css">
    </head>
    <body>
        <div class="box main-header-box">
            <img src="img/weed.png" alt="Italian Trulli">
            <p>This is the main image</p>
        </div>
        <h1> Kronink weedshop</h1>
        <br>
        <br>
        <div class="box lpagelogin">
            <form action="html/create_account.html">
                <button type="submit">Create Account</button>
            </form>
            <form action="html/login.html">
                <button type="submit">Log in</button>
            </form>
            <form action="php/logout.php" method="POST">
                <button name="submit" value= "guest" type="submit">Continue as guest</button>
            </form>
        </div>
        
        <footer class="box footer">
            <p>This is the footer box</p>
        </footer>

        <br>
        <br>
    </body>
</html>