<?php
include "input_table.php";

function    change_pw($login, $oldpw, $newpw)
{
    $newpw = hash('Whirlpool', $newpw, false);
    $oldpw = hash('Whirlpool', $oldpw, false);
    $exar = select_q("users", "passwd", "user", $login);
    
    if ($exar == $oldpw)
    {
        update_q("users", "passwd", $newpw, "user", $login);
        echo "Succesfull pw update";
        header("Location: ../html/login.html");
    }
    else
    {
        echo ("ERROR --> provided old password is wrong\n");
        exit ;
    }
    echo ("ERROR --> login name not found in database\n");
        exit ;
}
if (isset($_POST['login']))
    $login = $_POST['login'];
if (isset($_POST['oldpw']))
    $oldpw = $_POST['oldpw'];
if (isset($_POST['newpw']))
    $newpw = $_POST['newpw'];
change_pw($login, $oldpw, $newpw);
?>