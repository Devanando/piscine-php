<!DOCTYPE html>
<html>
<?php 
session_start();
include "input_table.php";

$id = $_SESSION['loggued_on_user'];
if (isset($_GET['buy']))
	add_basket($id, $_GET["buy"]);
if (isset($_GET["delete"]))
{
	if ($_GET["delete"] == "all")
		delete_basket($id);
	else
		delete_basket_item($_GET["delete"]);
}
$arr = get_basket($id);
$amount = 0;
$amount = amount_product($arr);
?>
    <head>
        <title>basket</title>
        <link rel="stylesheet" type="text/css" href="../css/global.css">
		<link rel="stylesheet" type="text/css" href="../css/webshop.css">
		<link rel="stylesheet" type="text/css" href="../css/products.css">
    </head>
    <body>
        <div class="box ws_head_box"><h1> Weed in a basket</h1></div>
        
        <div class="box menu">
                <form action="../php/webshop.php" method="POST">
                    <button class="dropbtn"type="submit">Homepage</button></form>
                <div class="dropdown">
                    <form action="../php/products.php" method="POST">
                    <button class="dropbtn">Products</button></form>
                    <div class="dropdown-content">
                        <a href="../php/products.php?category=sativa">Sativa</a>
                        <a href="../php/products.php?category=indica">Indica</a>
						<a href="../php/products.php?category=bio">Bio</a>
						<a href="../php/products.php?category=hybrid">Hybrid</a>
                    </div>
                </div>
                <div class="dropdown">
                    <form action="../php/basket.php" method="POST">
                    <button class="dropbtn">Basket <?php echo " ($amount)" ?></button></form>
                    <div class="dropdown-content">
                        <a href="../php/basket.php?checkout=sure">Checkout page</a>
                        <a href="../php/basket.php?delete=all">Empty basket</a>
                    </div>
                </div>
				<?php 
				if ($id != "")
                { 
					?>
                <div class="dropdown">
                    <form action="../html/create_account.html" method="POST">
                    <button class="dropbtn">account</button></form>
                    <div class="dropdown-content">
                        <a href="account_info.php">Show account info</a>
                        <a href="account_mng.php">Account management</a>
                    </div>
				</div><?php } 
				?>

        </div>
        <div class="box view_port">
		<?php
		if (isset($_GET['checkout']) && $_GET['checkout'] == "yes")
			{
				transfer_basket($id);
				?>
				Thank you for your order!! <br />
				Your orders will never be sent.
				 <div class="keepbig"></div> <?php
			}
		else{ 
			?>
		<div class="basket_items">
		<?php 
			if (isset($_GET['checkout']) && $_GET["checkout"] == "sure")
			{
				?>
				<a href="../php/basket.php?checkout=yes"><div class="checkout">Checkout Products</div></a>
				<a href="../php/basket.php?checkout=yes"><div class="checkout"></div></a>
			<?php
            }
            $arr = get_basket(($id));
			foreach ($arr as $product)
			{
				$product_name = select_q("products", "product", "id", $product["productid"]);?>
			    <a href="../php/products.php?id=<?php echo ($product["productid"]); ?>"><div class="basket_item"><?php echo ("Product : ".$product_name); ?> </div></a>
			    <a href="../php/basket.php?delete=<?php echo ($product["id"]); ?>"><div class="buybutbas">Remove from basket</div></a>
            <?php }} ?>
		</div>
        <?php
        if ($_SESSION['loggued_on_user'] != "")
        {
            ?>
            <div class="box ws-act-box ui"><form action="../php/logout.php" method="POST">
            <button type="submit">Log out</button>
        </form>
        <form action="../html/change_pw.html" method="POST">
        <button type="submit">Change password</button>
    </form>
    <form action="../index.php">
    <button type="submit">Go back to landing page</button>
</form></div>
<?php
        }
        else
        {
            ?>
                 <div class="box ws-act-box ui">
                     <form action="../html/create_account.html">
                     <button type="submit">Create Account</button>
                    </form>
                    <form action="../html/login.html">
                    <button type="submit">Log in with account</button>
                </form>
                <form action="../index.php">
                <button type="submit">Go back to landing page</button>
            </form></div><?php
        }
        ?>
        <div class="box footer">
            <div id="logged_status">Youz are currently logged in as <?php 
        if ($_SESSION['loggued_on_user'] != "")
        echo($_SESSION['loggued_on_user']);
        else
        echo 'guest'; ?></div>
        <div id="logged_status">&copy;Coffeeshop Kronink 2019</div>
    </div>
    </div>
    </body>
</html>