<!DOCTYPE html>
<html> <?php
    session_start();
    $id = $_SESSION['loggued_on_user']; ?>
    <head>
        <title>weedshop</title>
        <link rel="stylesheet" type="text/css" href="../css/global.css">
        <link rel="stylesheet" type="text/css" href="../css/webshop.css">
    </head>
    <body>
        <div class="box ws_head_box"><h1> Welcome to the weed</h1></div>
        
        <div class="box menu">
                <form action="webshop.php" method="POST">
                    <button class="dropbtn"type="submit">Homepage</button></form>
                <div class="dropdown">
                    <form action="../php/products.php" method="POST">
                    <button class="dropbtn">Products</button></form>
                    <div class="dropdown-content">
                        <a href="../php/products.php?category=sativa">Sativa</a>
                        <a href="../php/products.php?category=indica">Indica</a>
                        <a href="../php/products.php?category=bio">Bio</a>
                        <a href="../php/products.php?category=hybrid">Hybrid</a>
                    </div>
                </div>
                <div class="dropdown">
                    <form action="../php/basket.php" method="POST">
                    <button class="dropbtn">Basket</button></form>
                    <div class="dropdown-content">
                        <a href="../php/basket.php?checkout=">Checkout page</a>
                        <a href="../php/basket.php?category=indica">Empty basket</a>
                    </div>
                </div>
                <?php if ($id != "")
                { ?>
                <div class="dropdown">
                    <form action="../php/create_account.php" method="POST">
                    <button class="dropbtn">account</button></form>
                    <div class="dropdown-content">
                        <a href="account_info.php">Show account info</a>
                        <a href="account_mng.php">Account management</a>
                    </div>
                </div> <?php } ?>
        </div>
        <div class="box view_port"><div class="keepbig"></div>
            <?php
        if ($_SESSION['loggued_on_user'] != "")
        {
            ?>
            <div class="box ws-act-box ui"><form action="../php/logout.php" method="POST">
            <button type="submit">Log out</button>
        </form>
        <form action="../html/change_pw.html" method="POST">
        <button type="submit">Change password</button>
    </form>
    <form action="../index.php">
    <button type="submit">Go back to landing page</button>
</form></div>
<?php
        }
        else
        {
            ?>
                 <div class="box ws-act-box ui">
                     <form action="../html/create_account.html">
                     <button type="submit">Create Account</button>
                    </form>
                    <form action="../html/login.html">
                    <button type="submit">Log in with account</button>
                </form>
                <form action="../index.php">
                <button type="submit">Go back to landing page</button>
            </form></div><?php
        }
        ?>
        <div class="box footer">
            <div id="logged_status">Youz are currently logged in as <?php 
        if ($_SESSION['loggued_on_user'] != "")
            echo($_SESSION['loggued_on_user']);
        else
            echo 'guest'; ?></div>
        <div id="logged_status">&copy;Coffeeshop Kronink 2019</div>
    </div>
    </div>
    </body>
</html>