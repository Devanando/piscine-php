<?php
include 'input_table.php';
if ($_POST['login'] == "" || $_POST['passwd'] == "" || $_POST['submit'] != "OK")
{
    header("Location: ../html/create_account.html");
    exit ;
}
$user = $_POST['login'];
$passw = $_POST['passwd'];
$f_name = "";
$l_name = "";
$email = "";
$access = 2;
$sessionid = $_POST['login'];
foreach($_POST as $key => $value)
{
    if ($key == 'firstname')
        $f_name = $value;
    if ($key == 'lastname')
        $l_name = $value;
    if ($key == 'email')
        $email = $value;
    if ($key != 'access')
        $access = $key;
}
$result = add_user($user, $passw, $f_name, $l_name, $email, $access, $sessionid);
if ($result == false)
{
    header("Location: ../html/create_again.html");
    exit ;
}
else
    header("Location: ../html/login.html");
?>