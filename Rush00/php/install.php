<?php
include 'input_table.php';

function create_shop() {
	$conn = connect_db("");
	$db = "shop";
	$sql = "CREATE DATABASE IF NOT EXISTS $db";

	if (mysqli_query($conn, $sql))
		echo "Database created<br />";
	else
		echo "Database creation error<br />";
	$conn = connect_db($db);
	$sql = "CREATE TABLE IF NOT EXISTS products(
		id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
		product VARCHAR(70) NOT NULL UNIQUE,
		price DECIMAL NOT NULL,
		descript VARCHAR(255),
		category VARCHAR(20),
		bio	BOOLEAN,
		imgpath VARCHAR(255))";
	
	if (mysqli_query($conn, $sql))
		echo "Shop Table created<br />";
	else
		echo "Shop Table creation error<br />";

	$sql = "CREATE TABLE IF NOT EXISTS users(
		id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
		user VARCHAR(70) NOT NULL UNIQUE,
		passwd VARCHAR(255),
		firstname VARCHAR(70),
		lastname VARCHAR(70),
		email VARCHAR(70),
		acces INT NOT NULL,
		sessionid INT)";
	
	if (mysqli_query($conn, $sql))
		echo "User Table created<br />";
	else
		echo "User Table creation error<br />";
	
	$sql = "CREATE TABLE IF NOT EXISTS basket(
		id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
		sessionid VARCHAR(70) NOT NULL,
		productid INT NOT NULL)";
	
	if (mysqli_query($conn, $sql))
		echo "Basket Table created<br />";
	else
		echo "Basket Table creation error<br />";

	$sql = "CREATE TABLE IF NOT EXISTS orders(
		id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
		orderid INT NOT NULL,
		sessionid VARCHAR(70) NOT NULL,
		productid INT NOT NULL)";
	
	if (mysqli_query($conn, $sql))
		echo "orders Table created<br />";
	else
		echo "Orders Table creation error<br />";
	mysqli_close($conn);
}

create_shop();
add_user("Jesse", "root", "Jesse", "Dunnink", "jdunnink@kronink.nl", 1, 1);
add_user("Devan", "root", "Devanando", "Kroeke", "jdunnink@kronink.nl", 2, 1);
add_product("Lemon Haze", 12.00, "Nice sativa weed with a small lemon touch", "Sativa", 0, "../images/lhaze");
add_product("Amnessia Haze", 11.00, "Insert generic text on how great weed is", "Sativa",0, "../images/ahaze");
add_product("Royal Solo", 10.00, "Insert generic text on how great weed is", "Indica", 0,"../images/rsolo");
add_product("White Widow", 10.00, "Insert generic text on how great weed is", "Indica", 0,"../images/widow");
add_product("OG kush", 13.00, "Insert generic text on how great weed is", "Indica", 0,"../images/ogkush");
add_product("G13 Haze", 13.00, "Insert generic text on how great weed is", "Sativa", 0,"../images/ghaze");
add_product("Jackberry Kush", 14.00, "Insert generic text on how great weed is", "Hybrid", 0,"../images/jbkush");

