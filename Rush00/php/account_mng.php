<?php
    session_start();
    include "input_table.php";

    function check_admin_access($username)
    {
        $access = select_qry('users', 'acces', 'user', $username);
        if ($access == 1)
            return true;
        return false;
    }

    function 	select_qry($table, $search, $key, $value)
    {
	    $db = "shop";
	    $sql = "SELECT $search FROM $table WHERE $key='$value'";
	    $conn = connect_db($db);
	    if ($test = mysqli_query($conn, $sql))
	    {
            $arr = mysqli_fetch_array($test);
            mysqli_close($conn);
		    return ($arr[$search]);
	    }
        mysqli_close($conn);
        return false;
    }

    function	get_users()
    {
        $db = "shop";
		$sql = "SELECT * FROM users";
	    $conn = connect_db($db);
	    if ($test = mysqli_query($conn, $sql))
	    {
		    $arr = [];
		    while ($row = mysqli_fetch_array($test))
			    $arr[] = $row;
            mysqli_close($conn);
		    return ($arr);
        }
        mysqli_close($conn);
		return false; 
    }

    function delete()
    {
        $array = get_users();
        foreach($array as $key)
        {
            echo $key['id'];
            echo ' ';
            echo $key['user'];
            echo '<br>';
        }
        echo '
        <form action="account_mng.php" method="POST">
            <button type="submit">Go back</button>
        </form>';
        echo '
        <form action="../php/account_mng.php" method="POST">
        username to remove: <input type="text" name="username"><br>
        <input type="submit" name="submit" value="del">
        </form>';
        exit ;
    }

    function	delete_user($username)
    {
        if (check_admin_access($username) == true)
            return false;
        $db = "shop";
	    $sql = "DELETE FROM users WHERE user='$username'";
	    $conn = connect_db($db);
	    if (mysqli_query($conn, $sql))
	    {
		    mysqli_close($conn);
		    return true ;
	    }
        mysqli_close($conn);
        return false;
    }

    function print_users($array)
    {
        foreach($array as $key)
        {
            echo $key['id'];
            echo ' ';
            echo $key['user'];
            echo '<br>';
        }
        echo '
            <form action="account_mng.php" method="POST">
                <button type="submit">Go back</button>
            </form>';
        echo '
            <form action="account_mng.php" method="POST">
                <button name="submit" value="deluser" type="submit">Delete user</button>
            </form>';
        exit ;
    }

    function	get_orders()
    {
        $db = "shop";
		$sql = "SELECT * FROM orders";
	    $conn = connect_db($db);
	    if ($test = mysqli_query($conn, $sql))
	    {
		    $arr = [];
		    while ($row = mysqli_fetch_array($test))
			    $arr[] = $row;
            mysqli_close($conn);
		    return ($arr);
        }
        mysqli_close($conn);
		return false; 
    }

    function print_orders($array)
    {
        echo 'orderid';
        echo ' ';
        echo 'productid';
        echo '<br>';
        foreach($array as $key)
        {
            echo $key['orderid'];
            echo ' ';
            echo $key['productid'];
            echo '<br>';
        }
        echo '
            <form action="account_mng.php" method="POST">
                <button type="submit">Go back</button>
            </form>';
        exit ;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Account management</title>
        <link rel="stylesheet" type="text/css" href="../css/global.css">
    </head>
    <body>
        <?php
            if ($_SESSION['loggued_on_user'] == "")
                header("Location: webshop.php");

            if (check_admin_access($_SESSION['loggued_on_user']) == false)              // root access: 1 regular access: 2
                header("Location: webshop.php");

            foreach($_POST as $key => $value)
            {
                if ($key == 'submit')
                {
                    if ($value == 'showorders')
                    {
                        $array = get_orders();
                        print_orders($array);
                    }
                    else if ($value == 'showusers')
                    {
                        $array = get_users();
                        print_users($array);
                    }
                    else if ($value == 'deluser')
                        delete();
                    else if ($value == 'del')
                    {
                        delete_user($_POST['username']);
                        $array = get_users();
                        print_users($array);
                    }
                }
            }
        ?>
        <h1>Account management</h1>

        <form action="account_mng.php" method="POST">
            <button name="submit" value="showorders" type="submit">show order history</button>
        </form>
        <form action="account_mng.php" method="POST">
            <button name="submit" value="showusers" type="submit">display active user accounts</button>
        </form>
        <form action="webshop.php" method="POST">
            <button type="submit">Go back</button>
        </form>
    </body>
</html>