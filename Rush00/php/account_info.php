<?php
    include "input_table.php";
    session_start();
    if ($_SESSION['loggued_on_user'] == "")
        header("Location: webshop.php");

    function 	select_qry($table, $search, $key, $value)
    {
	    $db = "shop";
	    $sql = "SELECT $search FROM $table WHERE $key='$value'";
	    $conn = connect_db($db);
	    if ($test = mysqli_query($conn, $sql))
	    {
            $arr = mysqli_fetch_array($test);
            mysqli_close($conn);
		    return ($arr[$search]);
	    }
        mysqli_close($conn);
        return false;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Account info</title>
        <link rel="stylesheet" type="text/css" href="../css/global.css">
        <link rel="stylesheet" type="text/css" href="../css/account_info.css">
    </head>
    <body>
        <h1>Account info:</h1>

        <p>username: <?php echo $_SESSION['loggued_on_user'] ?></p>
        <p>first name:
            <?php
                echo ($firstname = select_qry('users', 'firstname', 'user', $_SESSION['loggued_on_user']));
            ?>
            <form action="mod_usr_data.php" method="POST">
                <button class="b_2" name="submit_init" value="firstname" type="submit">change first name</button>
            </form>
        </p>
        <p>last name:
            <?php
                echo ($lastname = select_qry('users', 'lastname', 'user', $_SESSION['loggued_on_user']));
            ?>
            <form action="mod_usr_data.php" method="POST">
                <button class="b_3" name="submit_init" value="lastname" type="submit">change last name</button>
            </form>
        </p>
        <p>email:
            <?php
                echo ($email = select_qry('users', 'email', 'user', $_SESSION['loggued_on_user']));
            ?>
            <form action="mod_usr_data.php" method="POST">
                <button class="b_4" name="submit_init" value="email" type="submit">change email</button>
            </form>
        </p>
        <form action="../html/change_pw.html" method="POST">
            <button type="submit">change password</button>
        </form>
        <form action="webshop.php" method="POST">
            <button type="submit">Go back</button>
        </form>
    </body>
</html>