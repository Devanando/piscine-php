<?php
$dir = "../private/";
$file = "passwd";
$login = $_POST['login'];
$passwd = $_POST['passwd'];
if ($_POST['submit'] == "OK" && $login != "" && $passwd != "")
{
	if (!file_exists($dir))
		mkdir($dir);
	if (!file_exists($dir.$file))
		file_put_contents($dir.$file, $arr);
	$user = unserialize(file_get_contents($dir.$file));
	foreach ($user as $key)
	{
		if ($key['login'] == $login)
		{
			echo "Error\n";
			exit();
		}
	}
	$tab['login'] = $login;
	$tab['passwd'] = hash("Whirlpool" , $passwd, false);
	$user[] = $tab;
	file_put_contents($dir.$file, serialize($user));
	echo "OK\n";
}
else
	echo "ERROR\n";
?>