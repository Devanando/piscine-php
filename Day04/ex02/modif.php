<?php
$dir = "../private/";
$file = "passwd";
$login = $_POST['login'];
$oldpw = $_POST['oldpw'];
$newpw = $_POST['newpw'];
if ($_POST['submit'] == "OK" && $login != "" && $oldpw != "" && $newpw != "")
{
	if (!file_exists($dir))
		mkdir($dir);
	if (!file_exists($dir.$file))
		file_put_contents($dir.$file, $arr);
	$user = unserialize(file_get_contents($dir.$file));
	foreach ($user as &$key)
	{
		if ($key['login'] == $login && $key['passwd'] == hash("Whirlpool", $oldpw, false))
		{
			$key['passwd'] = hash("Whirlpool", $newpw, false);
			file_put_contents($dir.$file, serialize($user));
			echo "OK\n";
			exit();
		}
	}
}
echo "ERROR\n";
?>