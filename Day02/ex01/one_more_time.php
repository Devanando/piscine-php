#!/usr/bin/php
<?php
$weekday = array(
	"monday" => "lundi" ,
	"Tuesday" => "mardi",
	"Wednesday" => "mercredi",
	"Thursday" => "jeudi",
	"Friday" => "vendredi",
	"Saturday" => "samedi",
	"Sunday" => "dimanche");

$month = array(
	 "Monday" => "janvier",
	"Tuesday" => "février",
	"Wednesday" => "mars",
	"Thursday" => "avril",
	"Friday" => "mai",
	"Saturday" => "juin",
	"Sunday" => "juilliet",
	"August" => "août",
	"September" => "septembre",
	"October" => "octobre",
	"November" => "novembre",
	"December" => "décembre");

$timestamp = strtolower($argv[1]);
$format = "D d M Y H:i:s";
date_default_timezone_set("Europe/Paris");
if (substr_count($timestamp, " ") == 4)
{
	$wday = strtok($timestamp, " ");
	if (in_array($wday, $weekday))
		$timestamp = str_replace($wday ,array_search($wday, $weekday), $timestamp);
	else
		exit("Invalid Day.\n");
	$tok = strtok(" ");
	if ((int)$tok > 31 || (int)$tok <= 0)
		exit("Invalid Month day.\n");
	$tok = strtok(" ");
	if (in_array($tok, $month))
		$timestamp = str_replace($tok ,array_search($tok, $month), $timestamp);
	else
		exit("Invalid Month format.\n");
	$tok = strtok(" ");
	if (strlen($tok) != 4)
		exit("Invalid Year format.\n");
	$tok = strtok(" ");
	if (!preg_match("/^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $tok))
		exit("Invalid Time\n");
	if (($testday == array_search($wday, $weekday)))
		echo "$wday should be $testday with given date.\n";
	if ($test == "")
		exit("Invalid Format.\n");
	else
		echo "$test\n";
}
else
{
	exit("Wrong Format.");
}