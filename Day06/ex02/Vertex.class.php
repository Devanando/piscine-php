<?php
require_once 'Color.class.php';

class Vertex
{
	private $_x;
    private $_y;
    private $_z;
    private $_w = 1.0;
	private $_color = false;
	
	static public $verbose = false;
	
    public function __construct(array $vertex)
    {
		if (!isset($vertex['x']) || !isset($vertex['y']) || !isset($vertex['z']) ||
			(isset($vertex['color']) && !($vertex['color'] instanceof Color)))
            return false;
        if (!isset($vertex['color'])) {
			$vertex['color'] = new Color(['red' => 255, 'green' => 255, 'blue' => 255]);}
        foreach ($vertex as $key => $value)
			$this->{"_$key"} = $value;
        if (self::$verbose)
			printf("%s constructed\n", $this->__toString());
			
        return true;
	}
	
    public function __destruct()
    {
        if (self::$verbose)
			printf("%s destructed\n", $this->__toString());
	}
	
    public function __toString()
    {
		$color = " )";
		if (self::$verbose)
			$color = ", $this->_color )";
		$string = sprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f%s",
					$this->_x, $this->_y, $this->_z, $this->_w, $color);
        return $string;
	}
	
    static public function doc(){
		$doc = sprintf("%s\n", file_get_contents('./Vertex.doc.txt'));
        return  $doc; }

    public function getX() {
        return $this->_x; }

	public function getY() {
		return $this->_y; }
	
	public function getZ() {
		return $this->_z; }

	public function getW() {
		return $this->_w; }

    public function setX($x) {
        $this->_x = $x; }
		
	public function setY($y) {
		$this->_y = $y; }
			
	public function setZ($z) {
		$this->_z = $z; }	

    public function setW($w) {
        $this->_w = $w; }
	
    public function getColor() {
        return $this->_color; }
	
    public function setColor(Color $color) {
        $this->_color = $color; }
	
    public function __get($name) {
        return false; }
	
    public function __set($name, $value) {
        return false; }
}