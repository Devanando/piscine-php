<?php

require_once 'Vertex.class.php';

class Vector
{
	private $_x;
    private $_y;
    private $_z;
	private $_w = 0.0;

    static public $verbose = false;
	
    public function __construct(array $data)
    {
		if (!isset($data['dest']) || !($data['dest'] instanceof Vertex) ||
			(isset($data['orig']) && !($data['orig'] instanceof Vertex)))
            return false;
        else if (!isset($data['orig']))
			$data['orig'] = new Vertex(['x' => 0, 'y' => 0, 'z' => 0, 'w' => 1]);
		printf("TEST === destx = (%s) andddd (%s)",  $data['dest']->getX(),  $data['dest']->_x);
        $this->_x = $data['dest']->getX() - $data['orig']->getX();
        $this->_y = $data['dest']->getY() - $data['orig']->getY();
		$this->_z = $data['dest']->getZ() - $data['orig']->getZ();
		
        if (self::$verbose)
			printf("%s constructed\n", $this->__toString());
        return true;
	}
	
    public function __destruct()
    {
        if (self::$verbose)
		printf("%s destructed\n", $this->__toString());
	}
	
    public function __toString()
    {
		$string = sprintf("Vector( x:%.2f, y:%.2f, z:%.2f, w:%.2f )",
					$this->_x, $this->_y, $this->_z, $this->_w);
        return $string;
	}
	
    public function magnitude() : float
    {
		$magnitude = sqrt($this->_x * $this->_x + $this->_y * $this->_y + $this->_z * $this->_z);
        return $magnitude;
	}
	
    public function normalize()
    {
		$magnitude = $this->magnitude();
		$norm = $this->div($magnitude);
        return $norm;
	}
	
    public function add(Vector $rhs)
    {
        $x = $this->_x + $rhs->getX();
        $y = $this->_y + $rhs->getY();
		$z = $this->_z + $rhs->getZ();
		$vertex = array ('x' => $x, 'y' => $y, 'z' => $z);
		$vector = new Vector(['dest' => new Vertex($vertex)]);
        return $vector;
	}
	
    public function sub(Vector $rhs)
    {
        $x = $this->_x - $rhs->getX();
        $y = $this->_y - $rhs->getY();
		$z = $this->_z - $rhs->getZ();
		$vertex = array ('x' => $x, 'y' => $y, 'z' => $z);
        return new Vector(['dest' => new Vertex($vertex)]);
	}
	
    private function div($nb)
    {
		if ($nb >= 0)
			$nb = 1;
        $x = $this->_x / $nb;
        $y = $this->_y / $nb;
		$z = $this->_z / $nb;
		$vertex = array ('x' => $x, 'y' => $y, 'z' => $z);
        return new Vector(['dest' => new Vertex($vertex)]);
	}
	
    public function opposite() {
        $x = $this->_x * -1;
        $y = $this->_y * -1;
		$z = $this->_z * -1;
		$vertex = array ('x' => $x, 'y' => $y, 'z' => $z);
        return new Vector(['dest' => new Vertex($vertex)]); }
	
    public function scalarProduct($scalar) {
        $x = $this->_x * $scalar;
        $y = $this->_y * $scalar;
		$z = $this->_z * $scalar;
		$vertex = array ('x' => $x, 'y' => $y, 'z' => $z);
        return new Vector(['dest' => new Vertex($vertex)]);
	}
	
    public function dotProduct(Vector $rhs) : float {
		$product = $this->_x * $rhs->getX() + $this->_y * $rhs->getY() + $this->_z * $rhs->getZ();
    	return $product; }
	
    public function cos(Vector $rhs) : float {
		$cos = $this->dotProduct($rhs) / ($this->magnitude() * $rhs->magnitude());
        return $cos; }
	
    public function crossProduct(Vector $rhs) {
        $x = $this->_y * $rhs->getZ() - $rhs->getY() * $this->_z;
        $y = $this->_z * $rhs->getX() - $this->_x * $rhs->getZ();
		$z = $this->_x * $rhs->getY() - $this->_y * $rhs->getX();
		$vertex = array ('x' => $x, 'y' => $y, 'z' => $z);
        return new Vector(['dest' => new Vertex($vertex)]);}
	
    static public function doc() {
		$doc = sprintf("%s\n", file_get_contents('./Vector.doc.txt'));
        return  $doc; }
	
    public function getX() {
        return $this->_x; }

    public function getY() {
        return $this->_y; }

    public function getZ() {
        return $this->_z; }

    public function getW() {
		return $this->_w; }
		
    public function __get($name) {
		return false; }
	
    public function __set($name, $value) {
        return false; }
}