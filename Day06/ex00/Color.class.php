<?php
class Color{

	public $red;
    public $green;
	public $blue;
    static public $verbose = false;
	
    public function __construct(array $color)
    {
        if (isset($color['rgb'])) {
            $this->red = intval(($color['rgb'] >> 16) & 0xFF, 10);
            $this->green = intval(($color['rgb'] >> 8) & 0xFF, 10);
            $this->blue = intval($color['rgb'] & 0xFF, 10);
		}
		else if (isset($color['red']) && isset($color['green']) && isset($color['blue'])) {
			$this->red = intval($color['red'], 10);
			$this->green = intval($color['green'], 10);
			$this->blue = intval($color['blue'], 10);
		}
		else
			return false;

        if (self::$verbose)
           printf("%s constructed.\n", $this->__toString());
        return true;
	}
	
    public function __destruct()
    {
        if (self::$verbose)
			printf("%s destructed.\n", $this->__toString());;
	}
	
    static public function doc()
    {
		$doc = sprintf("%s\n", file_get_contents('./Color.doc.txt'));
        return  $doc;
	}
	
    public function __toString()
    {
		$string = sprintf("Color( red: %3d, green: %3d, blue: %3d )", $this->red, $this->green, $this->blue);
        return $string;
	}
	
    public function add(Color $color)
    {
        $col = new Color(['red' => $this->red + $color->red,
            			'green' => $this->green + $color->green,
						'blue' => $this->blue + $color->blue]);
		return $col;
	}
	
    public function sub(Color $color)
    {
        $col = new Color(['red' => $this->red - $color->red,
            			'green' => $this->green - $color->green,
						'blue' => $this->blue - $color->blue]);
		return $col;
	}
	
    public function mult($mult)
    {
        $col = new Color(['red' => $this->red * $mult,
        				'green' => $this->green * $mult,
						'blue' => $this->blue * $mult]);
		return $col;
    }
}
?>